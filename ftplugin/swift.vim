" Indentation
set autoindent
set expandtab                           " Tabs are spaces, not tabs
set shiftround                          " Use multiple of shiftwidth when indenting with '<' and '>'
set shiftwidth=4                        " Use indents of 4 spaces
set smartindent
set smarttab
set softtabstop=4                       " Number of spaces that a <tab> counts for
set tabstop=4                           " An indentation every four columns


let g:loaded_syntastic_swift_swiftpm_checker = 1

let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let b:syntastic_mode = "passive"


