" Function to tell us the highlight group syntax
function! SyntaxItem()
    let l:name = synIDattr(synID(line("."),col("."),1),"name")
    if l:name == ''
        return ''
    endif
    return '[' . l:name . ']'
endfunction

" Strip trailing whitespace (,sw)
function! StripWhitespace ()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    :%s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfunction


" Source: http://vim.wikia.com/wiki/VimTip165
" Cleanly deletes a buffer without messing up the window layout.
" Modified by Evan Coury to prompt for unsaved changes.
function! s:Kwbd(kwbdStage)
  if(a:kwbdStage == 1)
    if(!buflisted(winbufnr(0)))
      bd!
      return
    endif
    let s:kwbdBufNum = bufnr("%")
    let s:kwbdWinNum = winnr()
    windo call s:Kwbd(2)
    execute s:kwbdWinNum . 'wincmd w'
    let s:buflistedLeft = 0
    let s:bufFinalJump = 0
    let l:nBufs = bufnr("$")
    let l:i = 1
    while(l:i <= l:nBufs)
      if(l:i != s:kwbdBufNum)
        if(buflisted(l:i))
          let s:buflistedLeft = s:buflistedLeft + 1
        else
          if(bufexists(l:i) && !strlen(bufname(l:i)) && !s:bufFinalJump)
            let s:bufFinalJump = l:i
          endif
        endif
      endif
      let l:i = l:i + 1
    endwhile
    if(!s:buflistedLeft)
      if(s:bufFinalJump)
        windo if(buflisted(winbufnr(0))) | execute "b! " . s:bufFinalJump | endif
      else
        enew
        let l:newBuf = bufnr("%")
        windo if(buflisted(winbufnr(0))) | execute "b! " . l:newBuf | endif
      endif
      execute s:kwbdWinNum . 'wincmd w'
    endif
    if(buflisted(s:kwbdBufNum) || s:kwbdBufNum == bufnr("%"))
      execute ":confirm :bd " . s:kwbdBufNum
    endif
    if(!s:buflistedLeft)
      set buflisted
      set bufhidden=delete
      set buftype=
      setlocal noswapfile
    endif
  else
    if(bufnr("%") == s:kwbdBufNum)
      let prevbufvar = bufnr("#")
      if(prevbufvar > 0 && buflisted(prevbufvar) && prevbufvar != s:kwbdBufNum)
        b #
      else
        bn
      endif
    endif
  endif
endfunction

command! Kwbd call s:Kwbd(1)
nnoremap <silent> <Plug>Kwbd :<C-u>Kwbd<CR>
nmap <silent> <leader>q :Kwbd<CR>


"----------------------------------------------------------------------------
" Move lines up / down : ctrl+shift+up/down
" See: http://stackoverflow.com/questions/741814/move-entire-line-up-and-down-in-vim
function! s:swap_lines(n1, n2)
    let line1 = getline(a:n1)
    let line2 = getline(a:n2)
    call setline(a:n1, line2)
    call setline(a:n2, line1)
endfunction

function! s:swap_up()
    let n = line('.')
    if n == 1
        return
    endif

    call s:swap_lines(n, n - 1)
    exec n - 1
endfunction

function! s:swap_down()
    let n = line('.')
    if n == line('$')
        return
    endif

    call s:swap_lines(n, n + 1)
    exec n + 1
endfunction

noremap <silent> <c-s-up> :call <SID>swap_up()<CR>
noremap <silent> <c-s-down> :call <SID>swap_down()<CR>




"----------------------------------------------------------------------------
" Append modeline after last line in buffer.
" Use substitute() instead of printf() to handle '%%s' modeline in LaTeX
" files.
function! AppendModeline()
  let l:modeline = printf(" vim: set tabstop=%d softtabstop=%d shiftwidth=%d %sexpandtab :",
        \ &tabstop, &softtabstop, &shiftwidth, &expandtab ? '' : 'no')
  let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
  call append(line("$"), '')
  call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>ml :call AppendModeline()<CR>


"----------------------------------------------------------------------------
" Show a liste of functions in the current file
function! ShowFunc(sort)
    let gf_s = &grepformat
    let gp_s = &grepprg
    if ( &filetype == "c" || &filetype == "php" || &filetype == "python" ||
                \ &filetype == "sh" )
        let &grepformat='%*\k%*\sfunction%*\s%l%*\s%f %m'
        let &grepprg = 'ctags -x --'.&filetype.'-types=f --sort='.a:sort
    elseif ( &filetype == "perl" )
        let &grepformat='%*\k%*\ssubroutine%*\s%l%*\s%f %m'
        let &grepprg = 'ctags -x --perl-types=s --sort='.a:sort
    elseif ( &filetype == "vim" )
        let &grepformat='%*\k%*\sfunction%*\s%l%*\s%f %m'
        let &grepprg = 'ctags -x --vim-types=f --language-force=vim --sort='.a:sort
    endif
    if (&readonly == 0) | update | endif
    silent! grep %
    cwindow 10
    redraw
    let &grepformat = gf_s
    let &grepprg = gp_s
endfunc


"----------------------------------------------------------------------------
" :Underline current line with character. e.g. :Underline -
function! s:Underline(chars)
  let chars = empty(a:chars) ? '=' : a:chars
  let nr_columns = virtcol('$') - 1
  let uline = repeat(chars, (nr_columns / len(chars)) + 1)
  put =strpart(uline, 0, nr_columns)
endfunction
command! -nargs=? Underline call s:Underline(<q-args>)


"----------------------------------------------------------------------------
"insert at start of current line by typing in __ (two underscores)
function DoubleUnderscore()
    if v:count == 0 && getcurpos()[2] == 1
        :silent call feedkeys('I', 'n')
    else
        :silent call feedkeys('^', v:count + 'n')
    endif
endfunction
nnoremap <silent> _ :call DoubleUnderscore()<CR>
