
" map jk and kj to escape
inoremap jk <Esc>
inoremap kj <Esc>

" Strip trailing whitespace (,sw)
noremap <leader>sw :call StripWhitespace ()<CR>

" // to find visually selected text
vnoremap // y/<C-R>"<CR>

" ,o to replace current word and set up so that . will do the same on the next
" instance of the current word
nmap <leader>o *Ncgn


" For when you forget to sudo.. Really Write the file.
cmap w!! w !sudo tee % >/dev/null

" control-tab to switch buffers
noremap <C-TAB>   :bnext<CR>
noremap <C-S-TAB> :bprev<CR>

" Make
noremap <S-C-b> :make<CR>

" clear matches using leader-space
nmap <leader><space> :noh<cr>


"Shortcut to auto indent entire file
"nmap <S-C-r> 1G=G``
"imap <S-C-r> <ESC>1G=Ga``

" Remove the Windows ^M - when the encodings gets messed up
nmap <Leader>M mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

"preview ctag
nmap <C-S-]> :exe "ptjump " . expand("<cword>")<Esc>
imap <C-S-]> :exe "ptjump " . expand("<cword>")<Esc>

" present a list when trying to jump to a ctag
nnoremap <C-]> g<C-]>

" Close every buffer
map <leader>qa :1,1000 bd!<cr>

" cclose to <leader>qq
map <leader>qq :cclose<cr>

" put the caret in a logical place after a visual mode yank
vmap y ygv<Esc>

" Switch between last two buffers
nnoremap <leader><leader> <C-^>

"What syntax highight is under the cursor?
map <F9> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

" shift+P should always paste from yank buffer
nnoremap <S-p> "0p

" Ctrl-a for select all
map <C-A> ggVG

" Ctrl+a to go to start of command line
cnoremap <C-A> <Home>

" highlight word under cursor, but don't move
" DOESN'T WORK nnoremap * :keepjumps normal! *``<cr>
nnoremap <silent> <Leader>* :let @/='\<<C-R>=expand("<cword>")<CR>\>'<CR>:set hls<CR>
nnoremap <silent> * :let @/='\<<C-R>=expand("<cword>")<CR>\>'<CR>:set hls<CR>

" Allow undo of insert mode's ctrl+w and ctrl+u
inoremap <c-u> <c-g>u<c-u>
inoremap <c-w> <c-g>u<c-w>

" PLUGINS


" Open in github
nmap gh :OpenGithubFile<CR>
vmap gh :OpenGithubFile<CR>

" Dash
:nmap <silent> <leader>h <Plug>DashSearch

" CtrlP keys
nmap <leader>t :CtrlP<cr>
nmap <leader>b :CtrlPBuffer<cr>
nmap <leader>k :CtrlPTag<cr>
nmap <leader>j :CtrlPBufTag<cr>

" Resize windows with arrow keys
nnoremap <C-k> <C-w>+
nnoremap <C-j> <C-w>-
nnoremap <C-h> <C-w><
nnoremap <C-l>  <C-w>>

" movement mapping {
" Delete yank or change until next UpperCase
" o waits for you to enter a movement command : http://learnvimscriptthehardway.stevelosh.com/chapters/15.html
" M is for Maj (as in french)
" :<c-u>execute -> special way to run multiple normal commande in a map : learnvimscriptthehardway.stevelosh.com/chapters/16.html

onoremap M :<c-u>execute "normal! /[A-Z]\r:nohlsearch\r"<cr>

" Display current syntax highlighting mode under cursor
map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<' . synIDattr(synID(line("."),col("."),0),"name") . "> lo<" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

" Show list of functions
noremap <F3> <Esc>:call ShowFunc("no")<CR><Esc>
noremap <S-F3> <Esc>:call ShowFunc("yes")<CR><Esc>


" ctrl+space to omnicomplete or autocomplete
inoremap <expr> <C-Space> pumvisible() \|\| &omnifunc == '' ?
\ "\<lt>C-n>" :
\ "\<lt>C-x>\<lt>C-o><c-r>=pumvisible() ?" .
\ "\"\\<lt>c-n>\\<lt>c-p>\\<lt>c-n>\" :" .
\ "\" \\<lt>bs>\\<lt>C-n>\"\<CR>"
imap <C-@> <C-Space>


" php_getset mappings
map <buffer> <leader>gsb :InsertBothGetterSetter<CR>
map <buffer> <leader>gsg :InsertGetterOnly<CR>
map <buffer> <leader>gss :InsertSetterOnly<CR>



" Ultisnips
if has('python')
  let g:UltiSnipsExpandTrigger="<tab>"
  let g:UltiSnipsJumpForwardTrigger="<c-b>"
  let g:UltiSnipsJumpBackwardTrigger="<c-z>"
endif


"Phpactor
if $VIMRC_ENABLE_PHPACTOR == 1
  " Include use statement
  nmap <Leader>u :call phpactor#UseAdd()<CR>

  " Include use statement
  nmap <F7> :call phpactor#FindReferences()<CR>

  " Invoke the context menu
  nmap <Leader>mm :call phpactor#ContextMenu()<CR>

  " Invoke the navigation menu
  nmap <Leader>nn :call phpactor#Navigate()<CR>

  " Goto definition of class or class member under the cursor
  nmap <Leader>o :call phpactor#GotoDefinition()<CR>

  " Transform the classes in the current file
  nmap <Leader>ptt :call phpactor#Transform()<CR>

  " Generate a new class (replacing the current file)
  nmap <Leader>pcc :call phpactor#ClassNew()<CR>

  " Extract expression (normal mode)
  nmap <silent><Leader>pee :call phpactor#ExtractExpression(v:false)<CR>

  " Extract expression from selection
  vmap <silent><Leader>pee :<C-U>call phpactor#ExtractExpression(v:true)<CR>

  " Extract method from selection
  vmap <silent><Leader>pem :<C-U>call phpactor#ExtractMethod()<CR>

endif


" Side Search
" SideSearch current word and return to original window
nnoremap <Leader>ss :SideSearch <C-r><C-w><CR>

" Create an shorter `SS` command
command! -complete=file -nargs=+ SS execute 'SideSearch <args>'

" php-refactoring-toolbox
nnoremap <unique> <Leader>rlv :call PhpRenameLocalVariable()<CR>
nnoremap <unique> <Leader>rcv :call PhpRenameClassVariable()<CR>
nnoremap <unique> <Leader>reu :call PhpExtractUse()<CR>
nnoremap <unique> <Leader>rrm :call PhpRenameMethod()<CR>
vnoremap <unique> <Leader>rec :call PhpExtractConst()<CR>
nnoremap <unique> <Leader>rep :call PhpExtractClassProperty()<CR>
vnoremap <unique> <Leader>rem :call PhpExtractMethod()<CR>
nnoremap <unique> <Leader>rcp :call PhpCreateProperty()<CR>
nnoremap <unique> <Leader>rdu :call PhpDetectUnusedUseStatements()<CR>
vnoremap <unique> <Leader>== :call PhpAlignAssigns()<CR>
nnoremap <unique> <Leader>rsg :call PhpCreateSettersAndGetters()<CR>
nnoremap <unique> <Leader>rcg :call PhpCreateGetters()<CR>
nnoremap <unique> <Leader>rda :call PhpDocAll()<CR>

" vim-test
nmap <silent> t<C-n> :TestNearest<CR>
nmap <silent> t<C-f> :TestFile<CR>
nmap <silent> t<C-s> :TestSuite<CR>
nmap <silent> t<C-l> :TestLast<CR>
nmap <silent> t<C-g> :TestVisit<CR>

