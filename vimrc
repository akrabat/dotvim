" vim: set tabstop=4 softtabstop=0 shiftwidth=4 noexpandtab :

set nocompatible
let $VIMHOME = split(&rtp, ',')[0]  " Find the vim path

" Vim-Plug
call plug#begin('~/.vim/bundle')
" dependency for something…
Plug 'mattn/webapi-vim'
Plug 'tomtom/tlib_vim'
" dependency for vim-test
Plug 'tpope/vim-dispatch'
" dependency for vim-rst-tables
" Plug 'ftplugin/vim_bridge'

Plug 'junegunn/vim-easy-align'
Plug 'tpope/vim-fugitive'
Plug 'tommcdo/vim-fubitive'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-commentary'
Plug 'ddrscott/vim-side-search'
Plug 'rking/ag.vim'
Plug 'scrooloose/syntastic'
Plug 'rizzatti/funcoo.vim'
Plug 'rizzatti/dash.vim'
Plug 'mattn/gist-vim'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdtree'
Plug 'keith/swift.vim'
Plug 'johnoshea/vim-twig'

" RST plugins
Plug 'nvie/vim-rst-tables'
Plug 'matthew-brett/vim-rst-sections'
Plug 'akrabat/vim-rst2pdfStyle'

Plug 'tpope/vim-repeat'
Plug 'gabrielelana/vim-markdown'
Plug 'kchmck/vim-coffee-script'
if has('python')
    Plug 'SirVer/ultisnips'
    Plug 'editorconfig/editorconfig-vim'
endif
if has('python3')
    Plug 'vim-vdebug/vdebug'
endif
Plug 'honza/vim-snippets'
Plug 'kylef/apiblueprint.vim'
if v:version > 800
    if $VIMRC_ENABLE_PHPACTOR == 1
        Plug 'phpactor/phpactor', {'for': 'php', 'do': 'composer install'}
    endif
endif
Plug 'phpstan/vim-phpstan'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'qpkorr/vim-bufkill'
if has('mac')
    Plug 'L-TChen/vim-dark-mode-switcher'
    Plug 'junegunn/vim-xmark'
endif
Plug 'gu-fan/riv.vim'
Plug 'drmikehenry/vim-extline'
Plug 'alvan/vim-php-manual'
Plug 'terryma/vim-multiple-cursors'
Plug 'StanAngeloff/php.vim'
Plug 'adoy/vim-php-refactoring-toolbox'
Plug 'vim-scripts/php_localvarcheck.vim'
Plug 'janko-m/vim-test'
Plug 'tpope/vim-obsession'
Plug 'dhruvasagar/vim-prosession'
Plug 'previm/previm'
call plug#end()

let mapleader = ","
let maplocalleader = ","

syntax on
filetype plugin indent on

" General settings
set autoread                            " Automatically reload changed files
set autowrite                           " Automatically save before :next, :make etc.
set backspace=indent,eol,start          " backspace
set backupcopy=yes                      " Preserve Finder labels on OS X
set backupdir=~/.vim.backup             " Directory for backup files when saving
set directory=~/.vim.backup             " Directory for swap files
set encoding=utf-8                      " UTF-8 is good
set formatoptions+=j                    " Delete comment character when joining commented lines
set hidden                              " Buffer switching without saving
set hlsearch                            " Highlight search terms
set ignorecase                          " Case insensitive search
set laststatus=2                        " Always display the status bar
set linebreak                           " Wrap long lines at a character in 'breakat'
"set mouse=a                             " Enable mouse usage
set mousemodel=popup                    " Enable context menu
set noerrorbells                        " No beeps
set nofoldenable                        " Turn off code folding
set novisualbell                        " No flashes
set number                              " Display line numbers
set ruler                               " Display line and column
set shortmess+=I                        " Disable splash text
set showcmd
set showmatch                           " Show matching brackets/parenthesis
set matchtime=1                         " Fast display of matching brackets/parenthesis
set smartcase                           " Case sensitive when an upper case letter is present
set splitbelow                          " Split horizontal windows below to the current windows
set splitright                          " Split vertical windows right to the current windows
set t_Co=256                            " 256 colours
set t_vb=                               " No flashes
if (has("termguicolors"))
 set termguicolors                      " vim 8 colours
endif
set timeoutlen=1200                     " Timeout for <leader>
set title                               " Change the terminal's title
set titleold=
set titlestring=Vim:\ %F
set undolevels=1000                     " Many levels of undo
set whichwrap+=<,>,h,l,[,]              " Allow cursor to move to next line
set scrolloff=5                         " Not quite the bottom or the top

let iterm_profile = $ITERM_PROFILE
if has('mac')
	" set background to dark or light based on Mojave's Dark Mode setting
	call darkmode#switch()
elseif iterm_profile =~ "dark"
	set background=dark
	colorscheme desert
else
	set background=light
endif

" Indentation
set autoindent
set expandtab                           " Tabs are spaces, not tabs
set shiftround                          " Use multiple of shiftwidth when indenting with '<' and '>'
set shiftwidth=4                        " Use indents of 4 spaces
set smartindent
set smarttab
set softtabstop=4                       " Number of spaces that a <tab> counts for
set tabstop=4                           " An indentation every four columns

" Clipboard
set clipboard=unnamed

" Display tabs and trailing spaces
set list
set listchars=tab:\⇥\ ,trail:·,extends:>,precedes:<,nbsp:+


" Completion
set wildmode=list:longest
set wildmenu                            " Enable ctrl-n and ctrl-p to scroll thru matches
set wildignore=*.o,*.obj,*~,*.so        " Stuff to ignore when tab completing
set wildignore+=*vim/backups*
set wildignore+=*sass-cache*
set wildignore+=*DS_Store*
set wildignore+=vendor/rails/**
set wildignore+=vendor/cache/**
set wildignore+=*.gem
set wildignore+=log/**
set wildignore+=tmp/**
set wildignore+=*.png,*.jpg,*.gif
set wildignore+=*.swp,*.zip,*.pdf,*.phar,*.class,*.pyc,*.bak
set wildignore+=*/build/**
set wildignore+=*/__CG__*

if v:version < 800
  set completeopt=menu,preview,longest
else
  set completeopt=menu,preview,noinsert,noselect,longest
endif

" make enter in omnicomplete act like C-y
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Grep
set grepprg=grep\ -nrI\ --exclude-dir=.vim\ --exclude-dir=.git\ --exclude=\"*.js.map\"\ --exclude=tags\ $*\ /dev/null

" use Ack for grep
" set grepprg=ack\ -H\ --nocolor\ --nogroup\ --column

" use Ag for grep
"set grepprg=ag\ --noheading\ --nocolor\ --nogroup\ --column

" Change cursor shape between insert and normal mode in iTerm2.app
if $TERM_PROGRAM =~ "iTerm"
    let &t_SI = "\<Esc>]50;CursorShape=1\x7" " Vertical bar in insert mode
    let &t_EI = "\<Esc>]50;CursorShape=0\x7" " Block in normal mode
endif


" Load other settings files
let vimsettings = '~/.vim/settings'
let uname = system("uname -s")

for fpath in split(globpath(vimsettings, '*.vim'), '\n')
  if (fpath == expand(vimsettings) . "/keymap-mac.vim") && uname[:4] ==? "linux"
    continue " skip mac mappings for linux
  endif

  if (fpath == expand(vimsettings) . "/keymap-linux.vim") && uname[:4] !=? "linux"
    continue " skip linux mappings for mac
  endif

  exe 'source' fpath
endfor


" Cursor
function s:SetCursorLine()
    set cursorline
    hi cursorline term=underline ctermbg=NONE guibg=NONE guifg=NONE
endfunction
set cursorline                          " Highlight the current line
augroup CursorLine
  au!
  au VimEnter,WinEnter,BufWinEnter * call s:SetCursorLine()
  au WinLeave * setlocal nocursorline
augroup END
" Status line
highlight StatusLine cterm=bold gui=bold ctermbg=black ctermfg=white guibg=black guifg=white
highlight StatusLineNC cterm=bold gui=bold ctermbg=lightgrey ctermfg=white guibg=lightgrey guifg=white


" =========================================================================
" Auto
" =========================================================================

" Close complete window
au CompleteDone * pclose


" Automatically open, but do not go to (if there are errors) the quickfix /
" location list window, or close it when is has become empty.
"
" Note: Must allow nesting of autocmds to enable any customizations for quickfix
" buffers.
" Note: Normally, :cwindow jumps to the quickfix window if the command opens it
" (but not if it's already open). However, as part of the autocmd, this doesn't
" seem to happen.
" autocmd QuickFixCmdPost [^l]* nested copen
" autocmd QuickFixCmdPost    l* nested lwindow

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
augroup jump_lastposition
    autocmd!
    autocmd BufReadPost *
            \ if line("'\"") > 0 && line("'\"") <= line("$") |
            \ exe "normal g`\"" |
            \ endif
augroup END

" Always start at top of file for a git commit message though
au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])
au FileType gitcommit au! BufEnter MERGE_MSG call setpos('.', [0, 1, 1, 0])


" *.inc files are PHP
augroup php_filetypes
    autocmd!
    autocmd BufNewFile,BufRead *.inc set ft=php
augroup END

" .md files are markdown
autocmd BufNewFile,BufReadPost *.md set filetype=markdown

" add json syntax highlighting
au BufNewFile,BufRead *.json set ft=javascript


" Enable project specific .vimrc files
set exrc
set secure

" update tags in background whenever you write a php file
au BufWritePost *.php silent! !eval '[ -f ".git/hooks/ctags" ] && .git/hooks/ctags' &

