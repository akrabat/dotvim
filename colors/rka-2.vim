" Vim color file
" Maintainer: Rob Allen<rob@akrabat.com>
" URL: https://akrabat.com

" Some tweaks to the default colour scheme

" Reset to default
set background=light
hi clear
if exists("syntax_on")
  syntax reset
endif

let colors_name = "rka-2"

" Highlight current line
" hi CursorLine term=NONE cterm=NONE ctermbg=254 guibg=grey90
hi CursorLine term=underline cterm=underline

" Status line
hi StatusLine guifg=#ffffff ctermfg=15 guibg=#464a4d ctermbg=239 gui=NONE cterm=NONE
hi StatusLineNC guifg=#464a4d ctermfg=239 guibg=#e0e0e0 ctermbg=254 gui=NONE cterm=NONE

" HTML
hi htmlItalic term=italic cterm=italic gui=italic
hi htmlBold   term=bold cterm=bold gui=bold
hi htmlH1     guifg=blue
hi Title      guifg=blue

" Markdown
hi mkdURL guifg=darkgreen
hi mkdCode guifg=black

" Diff
hi diffRemoved guifg=tomato
hi diffAdded guifg=limegreen

" Standard
hi String guifg=darkgreen ctermfg=24 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Constant guifg=black ctermfg=0 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
