" Status line

" default the statusline to green when entering Vim
hi statusline guibg=DarkGreen ctermfg=8 guifg=White ctermbg=15
" hi statuslineNC guibg=Black ctermfg=8 guifg=White ctermbg=15 gui=italic

" Formats the statusline
set statusline=%f                           " file name
" set statusline+=\ [%{strlen(&fenc)?&fenc:'none'}, "file encoding
" set statusline+=%{&ff}] "file format
set statusline+=%y      "filetype
set statusline+=%h      "help file flag
set statusline+=%m      "modified flag
set statusline+=%r      "read only flag

" display the syntax item name under cursor on the status line
" set statusline+=%{SyntaxItem()}

" Puts in the current git status
set statusline+=%{fugitive#statusline()}

" Puts in syntastic warnings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" set statusline+=\ %{SyntaxItem()}

set statusline+=\ %=                        " align left
set statusline+=Line:%l/%L                  " line X of Y
set statusline+=\ Col:%c                    " current column
set statusline+=\ Buf:%n                    " Buffer number
" set statusline+=\ [%b][0x%B]\               " ASCII and byte code under cursor


