" vim: set tabstop=4 softtabstop=0 shiftwidth=4 expandtab :

" NOTE: http://superuser.com/questions/319591/how-can-i-prevent-macvim-from-showing-os-x-find-replace-dialog-when-pressing-co

set lsp=2

set guifont=Menlo:h12

" Open sidebar with shift+cmd+1
map <S-D-1> :NERDTreeTabsToggle<CR>


" Disable MacVim save shortcut
macmenu File.Save key=<nop>

" Exit to Normal mode upon [Cmd+S]
inoremap <D-s> <Esc>:w<CR><Right>
vnoremap <D-s> <Esc>:w<CR>

" Save in Normal mode (block Substitute)
nnoremap <D-s> :w<CR>

if has("gui_macvim")
    let macvim_hig_shift_movement = 1
    set selectmode= 

    "use system clipboard
    set clipboard=unnamed
endif

" Turn off visual bell
autocmd! GUIEnter * set vb t_vb=

if &background =~# 'dark'
    colorscheme desert
endif
