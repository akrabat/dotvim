" Plugin configuration

" netrw
" See: https://shapeshed.com/vim-netrw/
let g:netrw_liststyle = 3
let g:netrw_banner = 0
let g:netrw_winsize = 25
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_list_hide = '.git'
" sort is affecting only: directories on the top, files below
let g:netrw_sort_sequence = '[\/]$,*'

"vim-easy-align
nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

" CtrlP
let g:ctrlp_root_markers = ['.ctrlp']
let g:ctrlp_clear_cache_on_exit = 0
" let g:ctrlp_max_files=0
let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/]\.(git|hg|svn)$',
    \ 'file': '\v\.(exe|so|dll)$',
    \ }
if executable('ag')
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
endif

" vim-gist
let g:gist_post_private = 1
let g:gist_show_privates = 1
let g:gist_detect_filetype = 1
let g:gist_open_browser_after_post = 1
let g:gist_clip_command = 'pbcopy'


" php_getset
let g:no_php_maps = 1  " map keys ourselve as we don't want <leader>b

" Syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" On check for php syntax errors. default is ['php' , 'phpcs', 'phpmd']
let g:syntastic_php_checkers=['php', 'phpcs']
" let g:syntastic_php_checkers=['php']
let g:syntastic_html_tidy_ignore_errors=['<link> escaping malformed URI reference']
let g:syntastic_php_phpcs_args='--standard=PSR2 -n'
let g:syntastic_perl_checkers = ['podchecker']
let g:syntastic_enable_swift_xcrun_checker=1
let g:syntastic_sh_shellcheck_args="-e SC1090 -e SC1091"
let g:syntastic_html_tidy_quiet_messages = { "level" : "warnings" }
let g:syntastic_twig_twiglint_exec = 'php'
let g:syntastic_twig_twiglint_exe = 'php ~/bin/twig-lint.phar'


" PHPStan
let g:phpstan_analyse_level = "max"


" NERDTree
map <leader>l :NERDTreeFind<CR>
map <leader>e :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen = 1



" Riv (rst)
let g:riv_disable_folding = 1
let g:riv_fold_level = 0
let g:riv_fold_auto_update = 0
let g:riv_auto_fold_force = 0

" PDV
" let g:pdv_template_dir = $HOME ."/.vim/bundle/pdv/templates"
" nnoremap <buffer> <leader>d :call pdv#DocumentCurrentLine()<CR>

" vim-buffkill
" disbale keyboard mappings
let g:BufKillCreateMappings = 0


" Phpactor
if $VIMRC_ENABLE_PHPACTOR == 1
  autocmd FileType php setlocal omnifunc=phpactor#Complete
  let g:phpactorOmniError = v:true
endif


" Side Search
" How should we execute the search?
" --heading and --stats are required!
let g:side_search_prg = 'ag --word-regexp'
  \. " --ignore='*.js.map'"
  \. " --heading --stats -B 1 -A 4"

" Can use `vnew` or `new`
let g:side_search_splitter = 'vnew'

" Percentage split
let g:side_search_split_pct = 0.8

" php-refactoring-toolbox
let g:vim_php_refactoring_use_default_mapping = 0
let g:vim_php_refactoring_make_setter_fluent = 1


" vim-commentary for Twig
autocmd FileType twig setlocal commentstring={#\ %s\ #}
autocmd FileType html.twig setlocal commentstring={#\ %s\ #}


" Vdebug
if exists("g:vdebug_options")
    let g:vdebug_options.path_maps = {"/vagrant": "/Users/rob/Projects/work/Howden/howden-portal"}
endif

" make test commands execute using dispatch.vim
let test#strategy = "dispatch"

"previm
let g:previm_open_cmd = 'open -a Safari'

