" Vim color file
" Maintainer: Rob Allen<rob@akrabat.com>
" Last Change: 20 April 2017
" URL: https://akrabat.com

" Some tweaks to the default colour scheme

" Reset to default
set background=light
hi clear
if exists("syntax_on")
  syntax reset
endif

let colors_name = "rka-default"

hi htmlItalic	term=italic cterm=italic gui=italic
hi htmlBold	term=bold cterm=bold gui=bold

" Highlight current line
" hi CursorLine term=NONE cterm=NONE ctermbg=254 guibg=grey90
hi CursorLine term=underline cterm=underline

hi Title guifg=blue

" HTML
hi htmlH1 guifg=blue

" Markdown
hi mkdURL guifg=darkgreen
hi mkdCode guifg=black

" Diff
hi diffRemoved guifg=tomato
hi diffAdded guifg=limegreen

" Standard
hi String guifg=darkgreen ctermfg=24 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Constant guifg=black ctermfg=0 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
